require 'curses'
require_relative 'grid'

include Curses

#starting_grid = []
#starting_grid << [false, false, false, false, false]
#starting_grid << [false, false, false, false, false]
#starting_grid << [false, true, true, true, false]
#starting_grid << [false, false, false, false, false]
#starting_grid << [false, false, false, false, false]
#
starting_data = []
starting_data << '                                      '
starting_data << '                         X            '
starting_data << '                       X X            '
starting_data << '             XX      XX            XX '
starting_data << '            X   X    XX            XX '
starting_data << ' XX        X     X   XX               '
starting_data << ' XX        X   X XX    X X            '
starting_data << '           X     X       X            '
starting_data << '            X   X                     '
starting_data << '             XX                       '
starting_data << '                                      '
starting_data << '                                      '

starting_grid = []
starting_data.each do |line|
  starting_grid_row = []
  line.each_char do |cell|
    starting_grid_row << (cell == 'X')
  end
  30.times { starting_grid_row << false }
  starting_grid << starting_grid_row
end
30.times { starting_grid << Array.new(69){ false } }

grid = Grid.new(ARGV[0])
grid.replace(starting_grid)
# grid.randomize(ARGV[1])

win  = Window.new(ARGV[0].to_i, ARGV[0].to_i, 0, 0)

loop do
  win.setpos(0, 0)
  win.addstr(grid.to_s)
  win.refresh
  # sleep(0.1)
  break unless grid.tick
end
