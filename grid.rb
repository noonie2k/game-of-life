require 'digest/md5'

class Grid < Array
  def initialize(size = 10)
    @size = size

    size.to_i.times do
      self << Array.new(size.to_i) { false }
    end
  end

  def randomize(randomness)
    random_grid = []
    @size.to_i.times do
      random_grid << Array.new(@size.to_i) { rand(1..randomness.to_i) == randomness.to_i }
    end

    self.replace(random_grid)
  end

  def tick
    new_grid = Grid.new(@size)

    self.each_with_index do |row, row_index|
      row.each_with_index do |cell, cell_index|
        neighbors = 0
        (-1..1).each do |row_offset|
          (-1..1).each do |cell_offset|
            neighbor_row = row_index + row_offset
            neighbor_cell = cell_index + cell_offset
            unless (row_offset == 0 && cell_offset == 0) || neighbor_row < 0 || neighbor_cell < 0 || neighbor_row == self.count || neighbor_cell == row.count
              if self[neighbor_row][neighbor_cell]
                neighbors += 1
              end
            end
          end
        end

        if cell && (neighbors < 2 || neighbors > 3)
          new_grid[row_index][cell_index] = false
        elsif cell == false && neighbors == 3
          new_grid[row_index][cell_index] = true
        else
          new_grid[row_index][cell_index] = cell
        end
      end
    end

    self.replace(new_grid)
  end

  def to_s
    output = ''
    self.each do |row|
      row.each do |cell|
        if cell
          output << 'X'
        else
          output << ' '
        end
      end
      output << "\n"
    end

    output
  end
end
